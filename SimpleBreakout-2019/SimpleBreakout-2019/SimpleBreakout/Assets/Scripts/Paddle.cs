﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour {

    public float maxSpeed;
    public float forceMultipler;

    float horizontalInput;
    Rigidbody rigidbodyComponent;

    void Awake() {
        // get references to components
        rigidbodyComponent = GetComponent<Rigidbody>();
    }

    void Update() {
        // record input
        horizontalInput = Input.GetAxisRaw("Horizontal");
    }

    void FixedUpdate() {
        // if paddle has not reached top speed
        if (Mathf.Abs(rigidbodyComponent.velocity.x) < maxSpeed) {
            // add force to the paddle if player is pressing left or right
            rigidbodyComponent.AddForce(new Vector3(horizontalInput * forceMultipler, 0, 0));
        }
    }
}
