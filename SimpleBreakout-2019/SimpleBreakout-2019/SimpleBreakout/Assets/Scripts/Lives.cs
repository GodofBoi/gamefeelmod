﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lives : MonoBehaviour
{
    //Contains the spheres that are protrayed as lives
    //public GameObject live1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //If the lives of the player reaches this number then get rid of a live in a cool way
        if(Ball.lives == 2)
        {
            iTween.PunchPosition(gameObject, new Vector3(2, 2, Random.Range(20, 40)), 2.0f);
            Destroy(gameObject, 0.5f);
        }
    }
}
