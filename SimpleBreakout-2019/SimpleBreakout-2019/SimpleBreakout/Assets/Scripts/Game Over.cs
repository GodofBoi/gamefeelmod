﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Ball.lives == 0)
        {
            SceneManager.LoadScene("Game Over", LoadSceneMode.Single);
            Debug.Log("Loading");
        }
    }
}
