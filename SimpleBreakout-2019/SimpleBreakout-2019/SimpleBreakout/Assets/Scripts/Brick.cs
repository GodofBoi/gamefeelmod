﻿using UnityEngine;

public class Brick : MonoBehaviour
{
    //Power up variables
    int powerUpChance;
    //public GameObject powerUpG;

    private void Awake()
    {
        //Creates a random number for the powerupchance int
        powerUpChance = Random.Range(0, 10);
    }

    void OnCollisionExit (Collision collision)
		{
        if(collision.gameObject.tag == "Ball")
        {
            //Adds onto the normal score when a ball collides with a brick
            ScoringSystems.Score++;

            //Adds to the combo score when a ball collides with the brick
            ScoringSystems.Combo++;

            iTween.PunchPosition(gameObject, new Vector3( 2, 2, Random.Range(50, 100)), 2.0f);
            Destroy(gameObject, 0.5f);

            //If powerupchance is equal to 5 then instantiate the powerup
            if(powerUpChance == 5)
            {
                //Instantiate(gameObject);
            }
        }
				
		}

}
