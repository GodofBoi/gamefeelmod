﻿using UnityEngine;

public class Ball : MonoBehaviour {

    public static float lives = 3;

    public static float speed = 5;
    Vector3 initialPosition;
    public float maxHitAngle;
    public AudioClip[] bounceSounds;

    AudioSource audioSourceComponent;
    Rigidbody rigidbodyComponent;


    void Awake() {
        // get references to components
        rigidbodyComponent = GetComponent<Rigidbody>();
        initialPosition = transform.position;
        audioSourceComponent = GetComponent<AudioSource>();
    }

    void Start() {
        // initially serve the ball directly upward
        rigidbodyComponent.velocity = new Vector3(0, -1, 0);
    }

    void FixedUpdate() {
        // fix the ball's velocity so that it is constant
        rigidbodyComponent.velocity = rigidbodyComponent.velocity.normalized * speed;
    }

    void Reset() {
        // reset the position of the ball to its initial position
        transform.position = initialPosition;
        // serve the ball in a random direction
        rigidbodyComponent.velocity = Random.insideUnitCircle;
    }

    void OnTriggerEnter(Collider other) {

        if (other.tag == "Dead Zone") {
            lives = lives - 1;
            Reset();
        }

    }

    void OnCollisionExit(Collision collision) {
        // play a bounce sound whenever hitting another object
        audioSourceComponent.PlayOneShot(bounceSounds[Random.Range(0, bounceSounds.Length)]);

        //If the ball hits the paddle the combo gets reset
        if(collision.gameObject.tag == "Player")
        {
            ScoringSystems.Combo = 0;
        }
    }

}
