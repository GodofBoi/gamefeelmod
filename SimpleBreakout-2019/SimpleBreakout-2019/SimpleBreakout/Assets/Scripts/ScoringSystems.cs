﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoringSystems : MonoBehaviour
{
    // Public and private variables to keep track of the player's score
    public static int Score;
    public string ScorePreFix;
    public Text ScoreCount;
    string stringformat = "00##";

    // Public and private variables to keep track of the player's combo score
    public static int Combo;
    public string ComboPreFix;
    public Text ComboCount;
    string stringformat2 = "00##";

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //As the socre raises an the game goes on the ball gains speed over score count
        if (Score == 0)
        {
            Ball.speed = 5;
        }

        if (Score >= 20)
        {
            Ball.speed = 6;
        }

        if (Score >= 50)
        {
            Ball.speed = 7;
        }

        if (Score >= 70)
        {
            Ball.speed = 8;
        }

        if (Score >= 90)
        {
            Ball.speed = 9;
        }

        if (Score >= 110)
        {
            Ball.speed = 10;
        }

        //Calls the UpdateHud function that controls the ui of the game
        UpdateHud();
    }

    void UpdateHud()
    {
        //A function that keeps track of the text within the ui object in game, changes the score and format
        ScoreCount.text = ScorePreFix + Score.ToString(stringformat);

        //A function that keeps track of the text within the ui object in game, changes the combo score and format
        ComboCount.text = ComboPreFix + Combo.ToString(stringformat2);
    }
}
